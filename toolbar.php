<style>
#toolso{position: fixed; right:0; top:4px; }
#toolso i{background: #fff; border:1px solid #e5e5e5;border-radius:5px;cursor:pointer;border-bottom:2px solid #e5e5e5;padding:5px}
#toolso i:hover{border-bottom:2px solid #ccc}
#toolso i:active{border-bottom:1px solid #e5e5e5}
img{border:1px solid #eee;}
</style>
<br>
<div id="toolso">
<i class="fa fa-bold fa-fw" id="bold" title="Bold Selected Text" alt="B"></i>
<i class="fa fa-underline fa-fw" id="underline" title="Underline Selected Text" alt="U"></i>
<i class="fa fa-strikethrough fa-fw" id="strike" title="Strike Selected Text" alt="S"></i>
<i class="fa fa-italic fa-fw" id="italic" title="Italic Selected Text" alt="I"></i>
<i class="fa fa-link fa-fw" id="alink" title="Insert Link on Selected Text" alt="L"></i>
<i class="fa fa-unlink fa-fw" id="rmlink" title="Remove Link on Selected Text" alt="Rl"></i>
<i class="fa fa-file-image-o fa-fw" id="imgins" title="Image Insert" alt="II"></i>
<i class="fa fa-file-code-o fa-fw" id="pre" title="Convert Line to Code"></i>
<i class="fa fa-header fa-fw" id="h1" title="Convert Line to Heading" ><sub>1</sub></i>
<i class="fa fa-header fa-fw" id="h2" title="Convert Line to SubHeading" ><sub>2</sub></i>
<i class="fa fa-arrows-alt fa-fw" id="smod" title="FullScreen : Save Before Doing it"></i>&nbsp;&nbsp;
</div>
<script>
document.getElementById("bold").addEventListener("click",function(){ document.execCommand("bold");});document.getElementById("underline").addEventListener("click",function(){document.execCommand("underline");});document.getElementById("italic").addEventListener("click",function(){document.execCommand("italic");});document.getElementById("strike").addEventListener("click",function(){document.execCommand("strikeThrough");});
document.getElementById("alink").addEventListener("click", function() {var linkURL = prompt("Enter a URL:", "http://"); var sText = document.getSelection(); document.execCommand('insertHTML', false, '<a href="' + linkURL + '" target="_blank">' + sText + '</a>'); });
document.getElementById("rmlink").addEventListener("click",function(){var e=document.getSelection();document.execCommand("unlink");});
document.getElementById("imgins").addEventListener("click",function(){var e=prompt("Enter Path to Image", "http://"); document.execCommand("insertImage", false, e);});
document.getElementById("h1").addEventListener("click",function(){document.execCommand('formatBlock', false, '<h2>');});
document.getElementById("h2").addEventListener("click",function(){document.execCommand('formatBlock', false, '<h4>');});
document.getElementById("pre").addEventListener("click",function(){document.execCommand('formatBlock', false, '<pre>');});
</script>
<?php
echo '<script>document.getElementById("smod").addEventListener("click",function(){location.href="cc.php?q='.$_GET['q'].'" })</script>';
?>